<?php

use Illuminate\Http\Request;
use App\Contact;

Route::group(['middleware'=>'api'],function(){

		Route::get('contacts',function(){

			return Contact::all();
		});

		Route::get('contact/{id}',function($id){

			return Contact::findOrFail($id);

		});

		Route::post('contact/store',function(Request $request){

			return Contact::create([
				'name'=>$request->name,
				'email'=>$request->email,
				'phone'=>$request->phone,]);


		});

		Route::patch('contact/{id}',function(Request $request,$id){

		  $lecontact = Contact::findOrFail($id);

		  $lecontact->name = $request->name;
		  $lecontact->email = $request->email;
		  $lecontact->phone = $request->phone;
		  $lecontact->save();

		});

		Route::delete('contact/{id}',function($id){

			return Contact::destroy($id); });

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


